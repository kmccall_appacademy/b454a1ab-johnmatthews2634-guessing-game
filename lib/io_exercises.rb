# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
  def guessing_game
    guessed_num = 0
    count = 0
    correct_number = 1 + rand(100)
    until guessed_num == correct_number
      puts "Guess a number between 1-100."
      guessed_number = gets.to_i
      guessed_num = guessed_number
      count += 1
      if guessed_num == correct_number
        puts "The number was #{correct_number}! You won in #{count} guesses!"
      elsif guessed_number > correct_number
        puts "#{guessed_number} is too high."
      elsif guessed_number < correct_number
        puts "#{guessed_number} is too low."
      end
    end
  end

  def file_shuffler(filename)
    base_filename = File.basename(filename, ".*")
    extension_name = File.extname(filename)

    File.open("#{filename}") do |line|
      line.gets
    end

    file_lines_array = File.readlines("#{filename}")
    file_lines_array.each do |line|
      line = line.chomp
    end

    shuffled_file = file_lines_array.shuffle

    new_filename = "#{base_filename}-shuffled#{extension_name}"
    File.open("#{new_filename}", "w") do |obj|
      shuffled_file.each do |line|
        obj.puts line
      end
    end

  end

  if __FILE__ == $PROGRAM_NAME
    if ARGV.length > 0
      file_shuffler(ARGV[0])
    else
      puts "Which file would you like to shuffle?"
      filename = gets.chomp
      file_shuffler(filename)
    end
  end
